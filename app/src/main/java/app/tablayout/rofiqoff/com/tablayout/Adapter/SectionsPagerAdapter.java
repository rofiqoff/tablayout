package app.tablayout.rofiqoff.com.tablayout.Adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import app.tablayout.rofiqoff.com.tablayout.Fragment.Fragment2;
import app.tablayout.rofiqoff.com.tablayout.Fragment.Fragment3;
import app.tablayout.rofiqoff.com.tablayout.Fragment.PlaceholderFragment;

/**
 * Created by RofiqoFF on 19/10/2016.
 */

public class SectionsPagerAdapter extends FragmentPagerAdapter {
    public SectionsPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        // getItem is called to instantiate the fragment for the given page.
        // Return a PlaceholderFragment (defined as a static inner class below).
        Fragment fragment = null;
        switch (position){
            case 0 :
                fragment = new PlaceholderFragment();
                break;
            case 1:
                fragment = new Fragment2();
                break;
            case 2:
                fragment = new Fragment3();
                break;
            default:
                fragment = new Fragment2();
        }

//        return PlaceholderFragment.newInstance(position + 1);
        return fragment;
    }

    @Override
    public int getCount() {
        // Show 3 total pages.
        return 3;
    }

    @Override
    public CharSequence getPageTitle(int position) {

        return null;
    }
}